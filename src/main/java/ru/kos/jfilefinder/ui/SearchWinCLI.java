package ru.kos.jfilefinder.ui;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.HashMap;
import java.util.Map;
import ru.kos.jfilefinder.Result;
import ru.kos.jfilefinder.Search;
import ru.kos.jfilefinder.Settings;

/**
 * Windows like CLI implementation
 * Main class, parameters are set here, and all results are returned here
 *
 * @author kosatchev
 */
public class SearchWinCLI implements SearchUI {

    private Search search;
    private Settings settings;
    private Result result;
    private long workTime;

    private static final String HELP = "-h";
    private static final String RECURSIVE = "-r";
    private static final String CASE_SENSITIVITY = "-c";
    private static final String SHOW_HIDDEN = "-a";

    private static final String DIRECTORY = "-d";
    private static final String NAME = "-n";
    private static final String OUTPUT = "-o";

    private static final String NAME_FULL = "-f";
    private static final String NAME_MASK = "-m";
    private static final String NAME_REGEX = "-e";

    private static final String VAL_MATCHER = "^("
            + DIRECTORY + "|"
            + NAME + "|"
            + OUTPUT + ")$";
    private static final String NON_VAL_MATCHER = "^("
            + RECURSIVE + "|"
            + CASE_SENSITIVITY + "|"
            + SHOW_HIDDEN + "|"
            + NAME_FULL + "|"
            + NAME_MASK + "|"
            + NAME_REGEX + ")$";

    private static final String HELP_TEXT = "jFileSearch 1.0 (2021-02-01). Usage:" + System.lineSeparator()
            + "JFileSearch.jar [-option a] [-option b] [-n name] [-d directory] [-o output]" + System.lineSeparator()
            + "e. g. java -jar JFileSearch.jar -d c:/ -n *.txt -m -o log.txt" + System.lineSeparator()
            + "-h - dosplay the jFileSearch help information" + System.lineSeparator()
            + "-r - recurse into directories" + System.lineSeparator()
            + "-c - case-sensitive search" + System.lineSeparator()
            + "-a - search hidden files and directories" + System.lineSeparator()
            + "-d [] - the directory to start the search in. By default, the current application directory" + System.lineSeparator()
            + "-n [] - name, mask or regular expression to search" + System.lineSeparator()
            + "-o [] - save result to file" + System.lineSeparator()
            + "The search type. If no parameter is specified it returns any matches" + System.lineSeparator()
            + "-f - full match" + System.lineSeparator()
            + "-m - mask search" + System.lineSeparator()
            + "-e - use regular expression" + System.lineSeparator()
            + "--help - display help for other interfaces";

    @Override
    public void init(Search search, String[] args) {
        this.search = search;
        this.settings = fillSettings(parseArgs(args));
    }

    @Override
    public void search() {
        this.result = new Result();
        if (settings.isHelp()) {
            showHelp();
        } else {
            long startTime = System.currentTimeMillis();
            search.search(settings, result);
            long endTime = System.currentTimeMillis();
            workTime = endTime - startTime;
            if (settings.getOutput() != null && !settings.getOutput().equals("")) {
                output();
            }
            printResult();
        }
    }

    @Override
    public void output() {

        File file = new File(settings.getOutput());
        File directory = new File(file.getParent());

        if (isCorrectPath(directory, file)) {
            try (BufferedWriter bw = new BufferedWriter(new FileWriter(settings.getOutput(), Charset.forName("UTF-8"), false))) {
                bw.append(fileListToString());
                bw.append(result.getMessage());
                result.appendMessage(String.format("Exported to %s", file.getCanonicalPath()));
            }
            catch (IOException e) {
                result.appendMessage(e.getMessage());
            }
        }
    }

    @Override
    public void showHelp() {
        System.out.println(HELP_TEXT);
    }

    private void printResult() {
        result.getFiles().forEach(System.out::println);
        result.appendMessage(System.lineSeparator() + "Work time: " + workTime);
        System.out.println(result.getMessage());
    }

    private boolean isCorrectPath(File directory, File file) {
        boolean rsl = false;

        if (file.exists()) {
            result.appendMessage(String.format("Cannot save to %s The file allready exists", directory.toString()));
        } else if (!directory.isDirectory()) {
            result.appendMessage(String.format("Cannot save to %s The directory does not exist", directory.toString()));
        } else {
            rsl = true;
        }
        return rsl;
    }

    private String fileListToString() {

        StringBuilder sb = new StringBuilder();

        result.getFiles().forEach(f -> {
            sb.append(f.toString());
            sb.append(System.lineSeparator());
        });
        return sb.toString();
    }

    private Map<String, String> parseArgs(String[] args) {
        Map<String, String> rsl = new HashMap<>();
        if (args.length > 0) {
            for (int i = 0; i < args.length; i++) {
                if (args[i].matches(VAL_MATCHER) && i + 1 < args.length) {
                    rsl.put(args[i], args[i + 1]);
                    i++;
                } else if (args[i].matches(NON_VAL_MATCHER)) {
                    rsl.put(args[i], "true");
                } else {
                    rsl.put(HELP, "true");
                    break;
                }
            }
        } else {
            rsl.put(HELP, "true");
        }
        return rsl;
    }

    private Settings fillSettings(Map<String, String> settings) {

        Settings rsl = new Settings();

        for (var key : settings.keySet()) {
            switch (key) {
                case HELP:
                    rsl.setHelp(true);
                    break;
                case RECURSIVE:
                    rsl.setRecursive(true);
                    break;
                case CASE_SENSITIVITY:
                    rsl.setCaseSensitivity(true);
                    break;
                case SHOW_HIDDEN:
                    rsl.setShowHidden(true);
                    break;
                case DIRECTORY:
                    rsl.setDirectory(settings.get(DIRECTORY));
                    break;
                case NAME:
                    rsl.setName(settings.get(NAME));
                    break;
                case OUTPUT:
                    rsl.setOutput(settings.get(OUTPUT));
                    break;
                case NAME_FULL:
                    rsl.setMask(Settings.Mask.FULL);
                    break;
                case NAME_MASK:
                    rsl.setMask(Settings.Mask.MASK);
                    break;
                case NAME_REGEX:
                    rsl.setMask(Settings.Mask.REGEX);
                    break;
                default:
                    break;
            }
        }
        return rsl;
    }

}
