package ru.kos.jfilefinder.ui;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ButtonGroup;
import javax.swing.GroupLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.LayoutStyle;
import javax.swing.WindowConstants;
import static javax.swing.WindowConstants.DISPOSE_ON_CLOSE;
import javax.swing.table.DefaultTableModel;
import ru.kos.jfilefinder.FileSearch;
import ru.kos.jfilefinder.Result;
import ru.kos.jfilefinder.Search;
import ru.kos.jfilefinder.Settings;

/**
 * Swing GUI implementation
 * Main class, parameters are set here, and all results are returned here
 *
 * @author kosatchev
 */
public class SearchSwing extends JFrame implements SearchUI {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SearchSwing().setVisible(true);
            }
        });
    }

    private Search search;
    private Settings settings;
    private Result result;
    private long workTime;

    private JTabbedPane nameAndPathTabbedPane;
    private JButton findButton;
//    private JButton stopButton;
    private JButton logExportButton;
    private JButton exitButton;
    private JButton helpButton;
    private JPanel settingsPanel;
    private JTextField searchTextField;
    private JTextField browseTextField;
    private JButton browseButton;
    private JCheckBox recursiveCheckBox;
    private JCheckBox caseSensivityCheckBox;
    private JCheckBox showHiddenCheckBox;
//    private JCheckBox jCheckBox4;
    private ButtonGroup buttonGroup1;
    private JRadioButton anyMatchRadioButton;
    private JRadioButton fullMatchRadioButton;
    private JRadioButton maskMatchRadioButton;
    private JRadioButton regexMatchRadioButton;
    private JTable resultTable;
    private DefaultTableModel model;
    private JScrollPane jScrollPane1;
    private JPanel logPanel;
    private JTextArea logTextArea;
    private JScrollPane jScrollPane3;
    private JLabel statusLabel;
    private JLabel counterLabel;

    /**
     * Creates new form NewApplication
     */
    public SearchSwing() {
        initComponents();

        //init
        FileSearch search = new FileSearch(); // search algorythm
        this.search = search;
        this.settings = new Settings();
    }

    @Override
    public void init(Search search, String[] args) {

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new SearchSwing().setVisible(true);
            }
        });

    }

    @Override
    public void search() {
        statusLabel.setText("Search");
        model.setRowCount(0);
        this.result = new Result();
        settings.setName(searchTextField.getText());
        settings.setDirectory(browseTextField.getText());
        settings.setRecursive(recursiveCheckBox.isSelected());
        settings.setCaseSensitivity(caseSensivityCheckBox.isSelected());
        settings.setShowHidden(showHiddenCheckBox.isSelected());
        settings.setMask(Settings.Mask.valueOf(buttonGroup1.getSelection().getActionCommand()));
        long startTime = System.currentTimeMillis();
        search.search(settings, result);
        long endTime = System.currentTimeMillis();
        workTime = endTime - startTime;
        result.getFiles().forEach(new Consumer<File>() {
            @Override
            public void accept(File e) {
                try {
                    String[] row = new String[4];
                    row[0] = e.getName();
                    row[1] = e.getCanonicalPath();
                    row[2] = Long.toString(e.length());
                    row[3] = Long.toString(e.lastModified());
                    model.addRow(row);
                } catch (IOException ex) {
                    Logger.getLogger(SearchSwing.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        });
        statusLabel.setText("Ready");
        counterLabel.setText("Find: " + String.valueOf(model.getRowCount()));
        logTextArea.append("Find: " + String.valueOf(model.getRowCount()) + "\n");
        logTextArea.append("Work time: " + workTime + "\n");
        logTextArea.append(settings + "\n");
        logTextArea.append(result + "\n");
    }

    @Override
    public void output() {
        String sb = "TEST CONTENT";
        JFileChooser chooser = new JFileChooser();
        chooser.setCurrentDirectory(new File(new File(SearchSwing.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent()));
        int retrival = chooser.showSaveDialog(null);
        if (retrival == JFileChooser.APPROVE_OPTION) {
            try (FileWriter fw = new FileWriter(chooser.getSelectedFile() + ".txt")) {
                fw.write(logTextArea.getText());
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
    }

    @Override
    public void showHelp() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private JDialog createDialog(String title, boolean modal) {
        JDialog dialog = new JDialog(this, title, modal);
        dialog.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        dialog.setSize(180, 90);
        return dialog;
    }

    @SuppressWarnings("unchecked")
    private void initComponents() {

        nameAndPathTabbedPane = new JTabbedPane();
        findButton = new JButton();
//        stopButton = new JButton();
        logExportButton = new JButton();
        exitButton = new JButton();
        helpButton = new JButton();
        settingsPanel = new JPanel();
        searchTextField = new JTextField();
        browseTextField = new JTextField();
        browseButton = new JButton();
        recursiveCheckBox = new JCheckBox();
        caseSensivityCheckBox = new JCheckBox();
        showHiddenCheckBox = new JCheckBox();
//        jCheckBox4 = new JCheckBox();
        buttonGroup1 = new ButtonGroup();
        anyMatchRadioButton = new JRadioButton();
        fullMatchRadioButton = new JRadioButton();
        maskMatchRadioButton = new JRadioButton();
        regexMatchRadioButton = new JRadioButton();
        resultTable = new JTable();
//        model = new DefaultTableModel();
        model = new DefaultTableModel() {
            // make all cells non editable
            @Override
            public boolean isCellEditable(int row, int column) {
                //all cells false
                return false;
            }
        };
        jScrollPane1 = new JScrollPane();
        logPanel = new JPanel();
        logTextArea = new JTextArea();
        jScrollPane3 = new JScrollPane();
        statusLabel = new JLabel();
        counterLabel = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        findButton.setText("Find");
        findButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                search();
            }
        });

//        stopButton.setText("Stop");

logExportButton.setText("Export log");
        logExportButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                output();
            }
        });

        exitButton.setText("Exit");
        exitButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        helpButton.setText("Help");
        helpButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JLabel label = new JLabel("JFileSearch. Swing GUI implementation");
                label.setHorizontalAlignment(JLabel.CENTER);
                JDialog dialog = createDialog("Модальное", true);
                dialog.add(label);
                dialog.pack();
                dialog.setLocationRelativeTo(null); 
                dialog.setSize(new Dimension(300, 200));
                dialog.setResizable(false);
                dialog.setVisible(true);
            }
        });

        nameAndPathTabbedPane.addTab("Name and path", settingsPanel);

        browseButton.setText("Browse");
        browseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
                int option = fileChooser.showOpenDialog(settingsPanel);
                if (option == JFileChooser.APPROVE_OPTION) {
                    try {
                        File directory = fileChooser.getSelectedFile();
                        browseTextField.setText(directory.getCanonicalPath());
                    } catch (IOException ex) {
                        Logger.getLogger(SearchSwing.class.getName()).log(Level.SEVERE, null, ex);
                    }
                } else {
                    browseTextField.setText("Open command canceled");
                }
            }
        });

        recursiveCheckBox.setText("Recursive search");

        caseSensivityCheckBox.setText("Case sensitivity");

        showHiddenCheckBox.setText("Show hidden");

//        jCheckBox4.setText("jCheckBox4");
        buttonGroup1.add(anyMatchRadioButton);
        buttonGroup1.add(fullMatchRadioButton);
        buttonGroup1.add(maskMatchRadioButton);
        buttonGroup1.add(regexMatchRadioButton);

        anyMatchRadioButton.setText("Any match");
        anyMatchRadioButton.setSelected(true);
        anyMatchRadioButton.setActionCommand("ANY");

        fullMatchRadioButton.setText("Full match");
        fullMatchRadioButton.setActionCommand("FULL");

        maskMatchRadioButton.setText("Mask search");
        maskMatchRadioButton.setActionCommand("MASK");

        regexMatchRadioButton.setText("Regular expression");
        regexMatchRadioButton.setActionCommand("REGEX");

        resultTable.setAutoCreateRowSorter(true); //add sorter

        // create a table model and set a Column Identifiers to this model 
        Object[] columns = {"Name", "Path", "Size", "Updated"};
        model.setColumnIdentifiers(columns);
        // set the model to the table
        resultTable.setModel(model);

        jScrollPane1.setViewportView(resultTable);

        nameAndPathTabbedPane.addTab("Log", logPanel);

        logTextArea.setColumns(20);
        logTextArea.setRows(5);
        logTextArea.setEditable(false);

        jScrollPane3.setViewportView(logTextArea);

        statusLabel.setText("Ready");

        counterLabel.setText("");

        setLayout();
    }

    private void setLayout() {
        GroupLayout settingsPanelLayout = new GroupLayout(settingsPanel);
        settingsPanel.setLayout(settingsPanelLayout);
        settingsPanelLayout.setHorizontalGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(settingsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                                .addComponent(searchTextField)
                                .addGroup(settingsPanelLayout.createSequentialGroup()
                                        .addComponent(browseTextField)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(browseButton))
                                .addGroup(settingsPanelLayout.createSequentialGroup()
                                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.TRAILING, false)
                                                .addComponent(recursiveCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(caseSensivityCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(showHiddenCheckBox, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        //                                                .addComponent(jCheckBox4, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        )
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                                .addComponent(anyMatchRadioButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(fullMatchRadioButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(maskMatchRadioButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                .addComponent(regexMatchRadioButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGap(152, 152, 152)))
                        .addContainerGap())
        );

        settingsPanelLayout.setVerticalGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(settingsPanelLayout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(searchTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(browseTextField, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addComponent(browseButton))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(recursiveCheckBox)
                                .addComponent(anyMatchRadioButton))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(caseSensivityCheckBox)
                                .addComponent(fullMatchRadioButton))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                .addComponent(showHiddenCheckBox)
                                .addComponent(maskMatchRadioButton))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(settingsPanelLayout.createParallelGroup(GroupLayout.Alignment.BASELINE)
                                //                                .addComponent(jCheckBox4)
                                .addComponent(regexMatchRadioButton))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane1, GroupLayout.DEFAULT_SIZE, 267, Short.MAX_VALUE)
                        .addContainerGap())
        );

        GroupLayout logPanelLayout = new GroupLayout(logPanel);
        logPanel.setLayout(logPanelLayout);
        logPanelLayout.setHorizontalGroup(
                logPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(logPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane3, GroupLayout.DEFAULT_SIZE, 721, Short.MAX_VALUE)
                                .addContainerGap())
        );

        logPanelLayout.setVerticalGroup(
                logPanelLayout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(logPanelLayout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jScrollPane3, GroupLayout.DEFAULT_SIZE, 445, Short.MAX_VALUE)
                                .addContainerGap())
        );

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(statusLabel)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(counterLabel))
                                .addComponent(nameAndPathTabbedPane))
                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                .addComponent(helpButton, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
                                .addComponent(exitButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                //                                                                .addComponent(stopButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(logExportButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(findButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())
        );

        layout.setVerticalGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                .addGroup(GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addComponent(findButton)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        //                                                                                .addComponent(stopButton)
                                        .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(logExportButton)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 414, Short.MAX_VALUE)
                                        .addComponent(exitButton))
                                .addComponent(nameAndPathTabbedPane))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(helpButton)
                                .addComponent(statusLabel)
                                .addComponent(counterLabel))
                        .addContainerGap())
        );
        pack();
    }

}
