package ru.kos.jfilefinder.ui;

import ru.kos.jfilefinder.Search;

/**
 * Main class, parameters are set here, and all results are returned here
 *
 * @author kosatchev
 */
public interface SearchUI {

    /**
     * Preparing settings class
     *
     * @param search
     * @param args
     */
    public void init(Search search, String[] args);

    /**
     * Perform search
     */
    public void search();

    /**
     * Output log
     */
    public void output();

    /**
     * Displays help
     */
    public void showHelp();

}
