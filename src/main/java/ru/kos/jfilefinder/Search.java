package ru.kos.jfilefinder;

/**
 * Interface of search algorythm
 *
 * @author kosatchev
 */
public interface Search {

    /**
     * Search with set parameters
     *
     * @param settings
     * @param result
     */
    public void search(Settings settings, Result result);

}
