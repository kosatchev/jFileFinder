package ru.kos.jfilefinder;

import java.io.File;

/**
 * Implementation of the search algorithm
 *
 * @author kosatchev
 */
public class FileSearch implements Search {

    private Result result;
    private Settings settings;

    @Override
    public void search(Settings settings, Result result) {

        init(settings, result);

        File directory;
        if (settings.getDirectory() == null || settings.getDirectory().equals("")) {
            directory = new File(new File(FileSearch.class.getProtectionDomain().getCodeSource().getLocation().getPath()).getParent());
        } else {
            directory = new File(settings.getDirectory());
        }

        if (!directory.exists()) {
            result.appendMessage(String.format("Not exist %s", directory.getAbsoluteFile()));
        } else if (!directory.isDirectory()) {
            result.appendMessage(String.format("Not directory %s", directory.getAbsoluteFile()));
        } else if (settings.isRecursive()) {
            recursiveSearch(directory);
        } else {
            planSearch(directory);
        }
    }
    
    private void init(Settings settings, Result result) {
        this.settings = settings;
        this.result = result;
    }

   
    private void planSearch(File directory) {
        for (File subfile : directory.listFiles()) {
            if (!subfile.isHidden() || settings.isShowHidden()) {
                if (matchFileName(subfile)) {
                    result.addFile(subfile);
                }
            }
        }
    }

    private void recursiveSearch(File directory) {
        if (!directory.isHidden() || settings.isShowHidden()) {
            if (directory.isDirectory()) {
//            System.out.println("Searching in ... " + directory.getAbsoluteFile());

                if (directory.canRead()) { // checking permissions on the file
                    for (File temp : directory.listFiles()) {
                        if (temp.isDirectory()) {
                            recursiveSearch(temp);
                        } else {
                            if (matchFileName(temp)) {
                                result.addFile(temp);
                            }
                        }
                    }
                } else {
                    result.appendMessage("\n" + directory.getAbsoluteFile() + "Access deied");
                }
            }
        }
    }
    
    private boolean matchFileName(File file) {
        boolean rsl = false;
        String name = settings.getName();
        String fileName = file.getName();
        if (!settings.isCaseSensitivity()) {
            name = name.toLowerCase();
            fileName = fileName.toLowerCase();
        }
        switch (settings.getMask()) {
            case MASK:
                if (name.startsWith("*") && fileName.endsWith(name.substring(1))) {
                    rsl = true;
                } else if (name.endsWith("*") && fileName.startsWith(name.substring(0, name.length() - 1))) {
                    rsl = true;
                } else if (name.equals("*")) {
                    rsl = true;
                }
                break;

            case FULL:
                if (fileName.equals(name)) {
                    rsl = true;
                }
                break;
            case REGEX:
                if (fileName.matches(name)) {
                    rsl = true;
                }
                break;
            case ANY:
            default:
                if (fileName.contains(name)) {
                    rsl = true;
                }
                break;
        }
        return rsl;
    }

}
