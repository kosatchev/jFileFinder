package ru.kos.jfilefinder;

/**
 * Search settings
 * contains the query and the search and output parameters
 *
 * @author kosatchev
 */
public class Settings {

    private boolean help;
    private boolean recursive;
    private boolean caseSensitivity;
    private boolean showHidden;

    private String directory;
    private String name;
    private String output;

    private Mask mask = Mask.ANY;

    public enum Mask {
        FULL,
        MASK,
        REGEX,
        ANY;
    }

    public void setHelp(boolean help) {
        this.help = help;
    }

    public void setRecursive(boolean recursive) {
        this.recursive = recursive;
    }

    public void setCaseSensitivity(boolean caseSensitivity) {
        this.caseSensitivity = caseSensitivity;
    }

    public void setShowHidden(boolean showHidden) {
        this.showHidden = showHidden;
    }

    public void setDirectory(String directory) {
        this.directory = directory;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOutput(String output) {
        this.output = output;
    }

    public void setMask(Mask mask) {
        this.mask = mask;
    }

    public boolean isHelp() {
        return help;
    }

    public boolean isRecursive() {
        return recursive;
    }

    public boolean isCaseSensitivity() {
        return caseSensitivity;
    }

    public boolean isShowHidden() {
        return showHidden;
    }

    public String getDirectory() {
        return directory;
    }

    public String getName() {
        return name;
    }

    public String getOutput() {
        return output;
    }

    public Mask getMask() {
        return mask;
    }

    @Override
    public String toString() {
        return "help: " + help + System.lineSeparator()
                + "recursive: " + recursive + System.lineSeparator()
                + "caseSensitivity: " + caseSensitivity + System.lineSeparator()
                + "showHidden: " + showHidden + System.lineSeparator()
                + "directory: " + directory + System.lineSeparator()
                + "name: " + name + System.lineSeparator()
                + "output: " + output + System.lineSeparator()
                + "mask: " + mask;
    }
}
