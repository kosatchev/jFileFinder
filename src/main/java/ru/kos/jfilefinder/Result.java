package ru.kos.jfilefinder;

import java.io.File;
import java.util.LinkedList;
import java.util.List;

/**
 * Search result Contains a collection of files and a message
 *
 * @author kosatchev
 */
public class Result {

    private final List<File> files;
    private final StringBuilder message;

    public Result() {
        this.files = new LinkedList<>();
        this.message = new StringBuilder();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        files.forEach(f -> sb.append(f).append(System.lineSeparator()));
        sb.append(System.lineSeparator())
                .append(message)
                .append(System.lineSeparator());
        return sb.toString();
    }

    public void appendMessage(String message) {
        this.message.append(message);
    }

    public void addFile(File file) {
        files.add(file);
    }

    public List<File> getFiles() {
        return files;
    }

    public String getMessage() {
        return message.toString();
    }

}
