package ru.kos.jfilefinder;

import ru.kos.jfilefinder.ui.SearchWinCLI;
import ru.kos.jfilefinder.ui.SearchSwing;
import ru.kos.jfilefinder.ui.SearchUI;

/**
 * Program starts here
 *
 * @author kosatchev
 */
public class Main {

    public static void main(String[] args) {

        SearchUI searchUI = null;
        Search search = new FileSearch(); // algorythm

        if (args.length == 0 || args[0].toLowerCase().equals("--help")) {
            System.out.println("File search in differetn interfaces\n"
                    + "use --swing to run in graphic mode\n"
                    + "use -h to view command-line parameters");
        } else if (args[0].toLowerCase().equals("--swing")) {
            searchUI = new SearchSwing(); // gui
        } else {
            searchUI = new SearchWinCLI(); // tui
        }

        searchUI.init(search, args); // interface initialisation
        searchUI.search(); // search and export

    }
}
