# TODO
- [ ] add Linux-like CLI
- [ ] choose the CLI depending on the host operating system
- [ ] make the list of results populate during the search process
- [ ] perform a search in a separate process